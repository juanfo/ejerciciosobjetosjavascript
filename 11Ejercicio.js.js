class libro {
    constructor(tituloLibro, autor, numEjemplares, numPrestados) {
        this.tituloLibro;
        this.autor;
        this.numEjemplares;
        this.numPrestados;
    }

    crear(tituloIngresado, autorIngresado, ejemIngresados, prestadosIngresados) {
        this.tituloLibro = tituloIngresado;
        this.autor = autorIngresado;
        this.numEjemplares = ejemIngresados;
        this.numPrestados = prestadosIngresados;
    }
    prestamo() {
        var disp = this.numEjemplares - this.numPrestados;
        if (disp > 0) {
            this.numPrestados += 1;
            return true;
        } else if (disp == 0) {
            return false;
        }
    }

    devolucion() {

        if (this.numPrestados > 0) {
            this.numPrestados -= 1;
            return true;
        } else if (this.numPrestados == 0) {
            return false;
        }
    }

    toString(libro) {
        return `Titulo: ${libro.tituloLibro}, Autor: ${libro.autor}, Número de ejemplares: ${libro.numEjemplares}, Ejemplares prestados: ${libro.numPrestados}`;

    }
}