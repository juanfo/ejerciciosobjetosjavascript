class Contador{
    constructor(numero){
        this.numero = numero;
    }

    valorActual(){
        return this.numero
    }

    incremento(inc){
        this.numero = this.numero + inc;
    }

    decremento(dec){
        this.numero = this.numero - dec;
    }

    reset(){
        this.numero = 0;
    }
}