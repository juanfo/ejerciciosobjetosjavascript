class Persona {
    constructor(nombre,edad,cedula) {
        this.nombre = nombre;
        this.edad = edad;
        this.cedula = cedula;
    }
    mostrar(){
        console.log("El nombre de la persona es: " + this.nombre);
        console.log("La edad de la persona es: " + this.edad);
        console.log("La cedula de la persona es: " + this.cedula);
    }
     es_mayor_de_edad(){
         
         if(this.edad > 18){
             console.log("Es mayor de edad" + ' ' + this.edad)
             
         }else
             console.log("No es mayor de edad" + ' ' +this.edad)
     }
}